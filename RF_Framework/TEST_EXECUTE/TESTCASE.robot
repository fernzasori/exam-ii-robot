*** Settings ***
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ------------------------------------------------------------------------------
Resource          ../KEYWORDS/Keywords.txt

*** Test Cases ***
WS2_ROBOT
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA1    2
    [W] Open Browser    https://www.advantageonlineshopping.com/#/
    [W] Capture Page Screenshot
    [W] Click Element    //a[@id='hrefUserIcon']//*[local-name()='svg']
    [W] Capture Page Screenshot
    Sleep    3
    [W] Input Text    //input[@name='username']    kachaporn
    [W] Capture Page Screenshot
    Sleep    3
    [W] Input Text    //input[@name='password']    Passw0rd
    [W] Capture Page Screenshot
    Sleep    3
    [W] Click Element    //sec-sender[@class='roboto-medium ng-isolate-scope sec-sender']
    [W] Capture Page Screenshot
    [W] Click Element    //div[@id='miceImg']
    [W] Capture Page Screenshot
    [W] Click Element    //img[@id='32']
    [W] Capture Page Screenshot
    [W] Click Element    //div[@id='Description']//div[2]//span[2]
    [W] Capture Page Screenshot
    [W] Input Text    //input[@name='quantity']    10
    [W] Capture Page Screenshot
    [W] Click Element    //button[@name='save_to_cart']
    [W] Capture Page Screenshot
    [W] Click Element    //a[@id='shoppingCartLink']//*[local-name()='svg']
    [W] Capture Page Screenshot
    [W] Click Element    //button[@id='checkOutButton']
    [W] Capture Page Screenshot
    Sleep    3
    [W] Check Meaasge    ORDER PAYMENT
    [W] Capture Page Screenshot
    [W] Click Element    //button[@id='next_btn']
    [W] Capture Page Screenshot
    [W] Click Element    //button[@id='pay_now_btn_MasterCredit']
    [W] Capture Page Screenshot
    [W] Check Meaasge    Thank you for buying with Advantage
    [W] Capture Page Screenshot
    [W] Click Element    //span[@class='hi-user containMiniTitle ng-binding']
    [W] Capture Page Screenshot
    [W] Click Element    //label[@class='option roboto-medium ng-scope'][contains(text(),'Sign out')]
    [W] Capture Page Screenshot
    [W] Close Browser
